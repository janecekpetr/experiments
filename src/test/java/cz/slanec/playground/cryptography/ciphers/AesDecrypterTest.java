package cz.slanec.playground.cryptography.ciphers;

import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.gitlab.janecekpetr.experiments.cryptography.ciphers.AesDecrypter;

class AesDecrypterTest {

	private static final String PLAINTEXT = "ahoj ahoj ahoj";
	private static final String PASSWORD = "ABCDEFGHIJKLMNOPQRSTUVWXYZ012245";

	public static void main(String[] args) throws GeneralSecurityException {
		byte[] key = PASSWORD.getBytes();
		SecretKey aesKey = new SecretKeySpec(key, "AES");
		byte[] iv = new byte[16];
		GCMParameterSpec ivSpec = new GCMParameterSpec(128, iv);

		Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
		cipher.init(Cipher.ENCRYPT_MODE, aesKey, ivSpec);
		byte[] ciphertext = cipher.doFinal(PLAINTEXT.getBytes());

		System.out.println("Plaintext: " + PLAINTEXT);
		System.out.println("Encrypted: " + Arrays.toString(ciphertext));
		System.out.println("Decrypted: " + new String(AesDecrypter.decrypt(ciphertext, key, iv), StandardCharsets.UTF_8));

		// you can make an instance and use it repeatedly:
		AesDecrypter decrypter = new AesDecrypter(key, iv);
		System.out.println(new String(decrypter.decrypt(ciphertext), StandardCharsets.UTF_8));
		System.out.println(new String(decrypter.decrypt(ciphertext), StandardCharsets.UTF_8));
		System.out.println(new String(decrypter.decrypt(ciphertext), StandardCharsets.UTF_8));
		System.out.println(new String(decrypter.decrypt(ciphertext), StandardCharsets.UTF_8));
	}

}