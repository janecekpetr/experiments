package com.gitlab.janecekpetr.experiments.scraps;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import com.gitlab.janecekpetr.experiments.scraps.MinStack;

class MinStackTest {

	private final MinStack stack = new MinStack();

	@Test
	void testEmpty() {
		assertThat(stack.min()).isEmpty();
		assertThat(stack.pop()).isEmpty();
	}

	@Test
	void testPushEmpty() {
		stack.push(5);

		assertThat(stack.min()).hasValue(5);
		assertThat(stack.pop()).hasValue(5);
	}

	@Test
	void testPushLess() {
		stack.push(5);
		stack.push(0);

		assertThat(stack.min()).hasValue(0);
		assertThat(stack.pop()).hasValue(0);
	}

	@Test
	void testPushGreater() {
		stack.push(5);
		stack.push(0);
		stack.push(10);

		assertThat(stack.min()).hasValue(0);
		assertThat(stack.pop()).hasValue(10);
	}

}