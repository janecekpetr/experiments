package com.gitlab.janecekpetr.experiments.selenium;

import java.nio.file.Paths;
import java.time.Duration;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

abstract class SeleniumTest {

	protected static WebDriver driver;
	protected static JavascriptExecutor js;

	@BeforeAll
	static void beforeClass() {
		Logger shutUp = Logger.getLogger("");
		shutUp.setLevel(Level.OFF);
	}

	@BeforeEach
	public void before() {
//		driver = new InternetExplorerDriver();
		driver = new FirefoxDriver();
//	    driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
		if (driver instanceof JavascriptExecutor) {
			js = (JavascriptExecutor)driver;
		}
		driver.manage().window().maximize();
	}

	@AfterEach
	public void after() {
		driver.close();
		js = null;
	}

	@AfterAll
	public static void afterClass() {
		driver.quit();
	}

	public static String file(String name) {
		return Paths.get("files",  name).toUri().toString();
	}

}