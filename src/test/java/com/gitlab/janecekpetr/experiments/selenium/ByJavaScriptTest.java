package com.gitlab.janecekpetr.experiments.selenium;

import java.util.List;

import com.gitlab.janecekpetr.experiments.selenium.MyBy;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

class ByJavaScriptTest extends SeleniumTest {

	@Test
	void byJavaScriptClass() {
		driver.get(file("Playground.html"));

		WebElement button = driver.findElement(By.tagName("button"));
		WebElement html = driver.findElement(By.tagName("html"));
		List<WebElement> divFromElementContext = button.findElements(MyBy.javascript("return document.querySelectorAll('div');"));
		List<WebElement> divFromElementContextHtml = html.findElements(MyBy.javascript("return document.querySelectorAll('div');"));
		WebElement buttonFromDriverContext = driver.findElement(MyBy.javascript("return document.querySelector('#button');"));

		System.out.println(buttonFromDriverContext.getText());
		System.out.println(divFromElementContext);
		System.out.println(divFromElementContextHtml);
	}

}