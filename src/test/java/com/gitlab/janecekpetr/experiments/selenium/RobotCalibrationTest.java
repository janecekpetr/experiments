package com.gitlab.janecekpetr.experiments.selenium;

import java.awt.Point;
import java.time.Duration;

import com.gitlab.janecekpetr.experiments.selenium.RobotCalibration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

class RobotCalibrationTest {

	private WebDriver driver;

	@BeforeEach
	void before() {
//		driver = new InternetExplorerDriver();
//		driver = new FirefoxDriver();

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		driver.manage().window().maximize();
	}

	@Test
	void calibrationTest() {
		Point p = RobotCalibration.calibrate(driver);
		System.out.println(p.x + ", " + p.y);
	}

	@AfterEach
	void after() {
		driver.quit();
	}

}