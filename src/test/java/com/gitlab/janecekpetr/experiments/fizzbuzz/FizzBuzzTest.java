package com.gitlab.janecekpetr.experiments.fizzbuzz;

import org.junit.jupiter.api.Test;

class FizzBuzzTest {

	@Test
	void fizzBuzz() {
		new FizzBuzz(
				new FizzBuzzCondition(3, "fizz"),
				new FizzBuzzCondition(5, "buzz"))
			.play(100);
	}

}