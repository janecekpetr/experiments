package com.gitlab.janecekpetr.experiments.datevalidation;

public class IntDateValidator implements DateValidator {
	private final int dateLength;
	
	public IntDateValidator(String pattern) {
		// TODO validate pattern to be correct
		this.dateLength = pattern.length();
	}

	@Override
	public boolean isDateValid(String dateString) {
		if (dateString == null || dateString.length() != dateLength) {
			return false;
		}
		
		int date;
		try {
			date = Integer.parseInt(dateString);
		} catch (NumberFormatException ignored) {
			return false;
		}
		
		int year = date / 10000;
		int month = (date % 10000) / 100;
		int day = date % 100;
		
		// leap years calculation not valid before 1581
		final boolean yearOk = (year >= 1581) && (year <= 2500);
		final boolean monthOk = (month >= 1) && (month <= 12);
		final boolean dayOk = (day >= 1) && (day <= DateValidationTest.daysInMonth(year, month));
		
		return (yearOk && monthOk && dayOk);
	}
}