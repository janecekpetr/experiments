package com.gitlab.janecekpetr.experiments.datevalidation;

public class DateValidationTest {
	private static final int MAX = 5_000_000;

	public static void main(String[] args) {
		String[] dates = {"1983", "99999999", "19831245", "19831130", "20051130", "20000228"};
		long time;	// no see
		int bogusCount = 0;

		time = System.currentTimeMillis();
		DateValidator setDateValidator = new SetDateValidator(1900, 2050);
		System.out.println("SetDateValidator (preparations): " + (System.currentTimeMillis() - time));
		
		time = System.currentTimeMillis();
		for (int i = 0; i < MAX; i++) {
			for (String date : dates) {
				if (setDateValidator.isDateValid(date)) {
					bogusCount++;
				}
			}
		}
		System.out.println("SetDateValidator: " + (System.currentTimeMillis() - time));
		
		DateValidator intDateValidator = new IntDateValidator("yyyyMMdd");
		time = System.currentTimeMillis();
		for (int i = 0; i < MAX; i++) {
			for (String date : dates) {
				if (intDateValidator.isDateValid(date)) {
					bogusCount++;
				}
			}
		}
		System.out.println("IntDateValidator: " + (System.currentTimeMillis() - time));
		
		System.out.println();
		System.out.println("Bogus, disregard it: " + bogusCount);
	}

	public static int daysInMonth(int year, int month) {
		int daysInMonth;
		switch (month) {
			case 1: // fall through
			case 3: // fall through
			case 5: // fall through
			case 7: // fall through
			case 8: // fall through
			case 10: // fall through
			case 12:
				daysInMonth = 31;
				break;
			case 2:
				if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
					daysInMonth = 29;
				} else {
					daysInMonth = 28;
				}
				break;
			default:
				// returns 30 even for nonexistant months
				daysInMonth = 30;
		}
		return daysInMonth;
	}
}