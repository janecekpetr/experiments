package com.gitlab.janecekpetr.experiments.datevalidation;

import java.util.HashSet;
import java.util.Set;

public class SetDateValidator implements DateValidator {
	private final Set<String> dates;
	
	public SetDateValidator(int minYear, int maxYear) {
		if (!checkYearInBound(minYear) || !checkYearInBound(maxYear)) {
			throw new IllegalArgumentException("Year must be between 0 and 10000.");
		}
		
		dates = new HashSet<>();
		
		for (int year = minYear; year < maxYear; year++) {
			for (int month = 1; month <= 12; month++) {
				for (int day = 1; day <= DateValidationTest.daysInMonth(year, month); day++) {
					StringBuilder date = new StringBuilder();
					if (year < 1000) {
						int tempYear = year;
						while (tempYear < 1000) {
							date.append('0');
							tempYear *= 10;
						}
					}
					date.append(year);
					if (month < 10) {
						date.append('0');
					}
					date.append(month);
					if (day < 10) {
						date.append('0');
					}
					date.append(day);
					dates.add(date.toString());
				}
			}
		}
	}
	
	
	@Override
	public boolean isDateValid(String dateString) {
		return dates.contains(dateString);
	}
	
	private static boolean checkYearInBound(int year) {
		return (year >= 1000 || year < 10000);
	}
}