package com.gitlab.janecekpetr.experiments.datevalidation;

public interface DateValidator {
	public boolean isDateValid(String dateString);
}