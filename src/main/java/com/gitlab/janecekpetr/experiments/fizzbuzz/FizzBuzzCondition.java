package com.gitlab.janecekpetr.experiments.fizzbuzz;

import java.util.function.IntPredicate;

public class FizzBuzzCondition implements IntPredicate, Comparable<FizzBuzzCondition> {

	private final int divisor;
	private final String message;

	public FizzBuzzCondition(int divisor, String message) {
		this.divisor = divisor;
		this.message = message;
	}

	@Override
	public boolean test(int number) {
		return isDivisible(number, divisor);
	}

	private static boolean isDivisible(int dividend, int divisor) {
		return (dividend % divisor == 0);
	}

	public String getMessage() {
		return message;
	}


	@Override
	public int compareTo(FizzBuzzCondition other) {
		return Integer.compare(divisor, other.divisor);
	}
}