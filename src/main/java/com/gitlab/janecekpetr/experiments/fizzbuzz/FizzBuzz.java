package com.gitlab.janecekpetr.experiments.fizzbuzz;

import static java.util.stream.Collectors.joining;

import java.util.stream.IntStream;

import com.google.common.collect.ImmutableSortedSet;

public class FizzBuzz {

	private final ImmutableSortedSet<FizzBuzzCondition> conditions;

	public FizzBuzz(FizzBuzzCondition... conditions) {
		this.conditions = ImmutableSortedSet.copyOf(conditions);
	}

	public FizzBuzz(Iterable<FizzBuzzCondition> conditions) {
		this.conditions = ImmutableSortedSet.copyOf(conditions);
	}

	public void play(int max) {
		IntStream.range(1, max + 1)
			.mapToObj(number -> {
				String messageFromConditions = conditions.stream()
					.filter(condition -> condition.test(number))
					.map(FizzBuzzCondition::getMessage)
					.collect(joining());
				if (!messageFromConditions.isEmpty()) {
					return messageFromConditions;
				}
				return String.valueOf(number);
			})
			.forEach(System.out::println);
	}
}