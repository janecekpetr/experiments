package com.gitlab.janecekpetr.experiments.selenium;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WrapsDriver;

class ByJavaScript extends By implements Serializable {
	private final String script;

	public ByJavaScript(String script) {
		checkNotNull(script, "Cannot find elements with a null JavaScript expression.");
		this.script = script;
	}

	@Override
	public List<WebElement> findElements(SearchContext context) {
		JavascriptExecutor js = getJavascriptExecutorFromSearchContext(context);

		// call the JS, inspect and validate response
		Object response = js.executeScript(script);
		List<WebElement> elements = getElementListFromJsResponse(response);

		// filter out the elements that aren't descendants of the context node
		if (context instanceof WebElement element) {
			filterOutElementsWithoutCommonAncestor(elements, element);
		}

		return elements;
	}

	private static JavascriptExecutor getJavascriptExecutorFromSearchContext(SearchContext context) {
		if (context instanceof JavascriptExecutor jsContext) {
			// context is most likely the whole WebDriver
			return jsContext;
		}
		if (context instanceof WrapsDriver outerDriver) {
			// context is most likely some WebElement
			WebDriver driver = outerDriver.getWrappedDriver();
			checkState(driver instanceof JavascriptExecutor, "This WebDriver doesn't support JavaScript.");
			return (JavascriptExecutor)driver;
		}
		throw new IllegalStateException("We can't invoke JavaScript from this context.");
	}

	@SuppressWarnings("unchecked")	// cast thoroughly checked
	private static List<WebElement> getElementListFromJsResponse(Object response) {
		if (response == null) {
			// no element found
			return List.of();
		}
		if (response instanceof WebElement element) {
			// a single element found
			return List.of(element);
		}
		if (response instanceof List) {
			// found multiple things, check whether every one of them is a WebElement
			checkArgument(
					Iterables.all((List<?>)response, Predicates.instanceOf(WebElement.class)),
					"The JavaScript query returned something that isn't a WebElement.");
			return (List<WebElement>)response;	// cast is checked as far as we can tell
		}
		throw new IllegalArgumentException("The JavaScript query returned something that isn't a WebElement.");
	}

	private static void filterOutElementsWithoutCommonAncestor(List<WebElement> elements, WebElement ancestor) {
		for (Iterator<WebElement> iter = elements.iterator(); iter.hasNext(); ) {
			WebElement elem = iter.next();

			// iterate over ancestors
			while (!elem.equals(ancestor) && !elem.getTagName().equals("html")) {
				elem = elem.findElement(By.xpath("./.."));
			}

			if (!elem.equals(ancestor)) {
				iter.remove();
			}
		}
	}

	@Override
	public String toString() {
	      return "By.javaScript: \"" + script + "\"";
	}
}