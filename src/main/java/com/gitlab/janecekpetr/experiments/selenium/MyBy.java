package com.gitlab.janecekpetr.experiments.selenium;

import static com.google.common.base.Preconditions.checkArgument;

import org.openqa.selenium.By;

public class MyBy {
	/**
	 * @param script
	 *        The JavaScript expression to run and whose result to return
	 * @return a By which locates elements by the JavaScript expression passed to it
	 */
	public static By javascript(String script) {
		checkArgument(script != null, "Cannot find elements with a null JavaScript expression.");
		return new ByJavaScript(script);
	}
}