package com.gitlab.janecekpetr.experiments.cryptography.ciphers;

import java.security.GeneralSecurityException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * A simple to use, no dependency required, reusable decrypter class for the AES-GCM algorithm with no padding.
 * <p>
 * This is intended to be used as a one-shot UDF provider for a server-side DB. If you wish to use this elsewhere,
 * please consider using a proven and well-tested high-level alternative, such as Google Keyczar, Apache Shiro, Spring
 * Security etc. Or at least write a nice abstraction around this and basic unit tests.
 */
public class AesDecrypter {
	/**
	 * Decrypts a given byte[] ciphertext using AES-GCM no padding and a provided IV.
	 *
	 * @param ciphertext An AES-GCM encoded ciphertext. Its length must be a multiple of 16 bytes (128 bits).
	 * @param key A secret key, must be 128, 192 or 256 bits long
	 *
	 * @throws NullPointerException If either ciphertext or key is null.
	 * @throws GeneralSecurityException If the underlying platform does not support AES-GCM algorithm,
	 * 		   the ciphertext or key have the wrong lengths
	 */
	public static byte[] decrypt(byte[] ciphertext, byte[] key, byte[] iv) throws GeneralSecurityException {
		return new AesDecrypter(key, iv).decrypt(ciphertext);
	}

	private final Cipher cipher;

	/**
	 * Creates a reusable decrypter instance for the AES-GCM algorithm with no padding and a provided IV that is capable of decrypting
	 * multiple ciphertexts using the same secret key.
	 *
	 * @param key A secret key, must be 128, 192 or 256 bits long
	 *
	 * @throws NullPointerException If the provided secret key is null.
	 * @throws GeneralSecurityException If the underlying platform does not support AES-GCM algorithm, orthe key has the wrong length
	 */
	public AesDecrypter(byte[] key, byte[] iv) throws GeneralSecurityException {
		if (key == null) {
			throw new NullPointerException("Please provide a non-null key for decryption.");
		}

		cipher = Cipher.getInstance("AES/GCM/NoPadding");

		SecretKey aesKey = new SecretKeySpec(key, "AES");
		GCMParameterSpec ivSpec = new GCMParameterSpec(128, iv);
		cipher.init(Cipher.DECRYPT_MODE, aesKey, ivSpec);
	}

	/**
	 * Decrypts a given byte[] ciphertext using AES-GCM with no padding and a provided IV.
	 *
	 * @param ciphertext An AES-GCM encoded ciphertext.
	 *
	 * @throws NullPointerException If the provided ciphertext is null.
	 * @throws GeneralSecurityException If the ciphertext has the wrong length
	 */
	public byte[] decrypt(byte[] ciphertext) throws GeneralSecurityException {
		if (ciphertext == null) {
			throw new NullPointerException("Please provide a non-null ciphertext for decryption.");
		}
		return cipher.doFinal(ciphertext);
	}
}