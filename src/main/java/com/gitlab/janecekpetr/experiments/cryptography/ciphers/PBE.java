package com.gitlab.janecekpetr.experiments.cryptography.ciphers;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class PBE {

	public static void main(String[] args) throws Exception {
		String ALGORITHM = "PBEWithMD5AndDES";
		String plaintext = "hello";
		String pass = "pass";
		
		PBEKeySpec passKey = new PBEKeySpec(pass.toCharArray());
		Key key = SecretKeyFactory.getInstance(ALGORITHM).generateSecret(passKey);

		Cipher cipher = Cipher.getInstance(ALGORITHM);
		
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] encoded = cipher.doFinal(plaintext.getBytes());
		
		cipher.init(Cipher.DECRYPT_MODE, key, cipher.getParameters());
		String decoded = new String(cipher.doFinal(encoded));
		
		System.out.println(decoded);
	}

}