package com.gitlab.janecekpetr.experiments.cryptography.ciphers;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class AES2 {

	public static void main(String[] args) throws Exception {
		aes256GcmNoPaddingNoSalt();
		gcmSalt();
	}

	private static void aes256GcmNoPaddingNoSalt() throws Exception {
		String plaintext = "testing message, blamblamblam";
		String pass = "ABCDEFGHIJKLMNOPQRSTUVWXYZ012245";

		SecretKey key = new SecretKeySpec(pass.getBytes(), "AES");

		SecureRandom rand = new SecureRandom();
		byte[] iv = new byte[16];
		rand.nextBytes(iv);
		GCMParameterSpec ivspec = new GCMParameterSpec(128, iv);

		Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
		cipher.init(Cipher.ENCRYPT_MODE, key, ivspec);
		byte[] encrypted = cipher.doFinal(plaintext.getBytes());

		cipher.init(Cipher.DECRYPT_MODE, key, ivspec);
		byte[] decrypted = cipher.doFinal(encrypted);

		System.out.println(new String(decrypted, StandardCharsets.UTF_8));
	}

	private static void gcmSalt() throws Exception {
		String plaintext = "testing message, blamblamblam";
		String pass = "YELLOW SUBMARINE";

		SecureRandom rand = new SecureRandom();
		byte[] iv = new byte[16];
		byte[] salt = new byte[8];
		rand.nextBytes(iv);
		rand.nextBytes(salt);
		GCMParameterSpec ivspec = new GCMParameterSpec(128, iv);

		KeySpec passKey = new PBEKeySpec(pass.toCharArray(), salt, 65536, 128);
		SecretKey secret = SecretKeyFactory.getInstance("PBKDF2withHmacSHA1").generateSecret(passKey);
		SecretKey key = new SecretKeySpec(secret.getEncoded(), "AES");

		Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
		cipher.init(Cipher.ENCRYPT_MODE, key, ivspec);
		byte[] encrypted = cipher.doFinal(plaintext.getBytes());

		cipher.init(Cipher.DECRYPT_MODE, key, ivspec);
		byte[] decrypted = cipher.doFinal(encrypted);

		System.out.println(new String(decrypted, StandardCharsets.UTF_8));
	}

}