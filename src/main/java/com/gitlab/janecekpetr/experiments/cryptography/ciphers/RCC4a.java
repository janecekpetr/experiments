package com.gitlab.janecekpetr.experiments.cryptography.ciphers;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

public class RCC4a {

	public static void main(String[] args) throws Exception {
		Key key = KeyGenerator.getInstance("RC4").generateKey();
		Cipher cipher = Cipher.getInstance("RC4");
		
		cipher.init(Cipher.ENCRYPT_MODE, key);
		String plainText = "Hello World of Encryption using RC4";
		byte[] byteCipherText = cipher.doFinal(plainText.getBytes());
		
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] byteDecryptedText = cipher.doFinal(byteCipherText);
		String strDecryptedText = new String(byteDecryptedText);
		
		System.out.println(strDecryptedText);
	}
	
}