package com.gitlab.janecekpetr.experiments.cryptography.ciphers;

import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

public class RCC4b {

	public static void main(String[] args) throws Exception {
		String plain = "testisperfect";
		Key key = getKey();
		String encrypted = encrypt(plain, key);
		System.out.println(encrypted);
		String decrypted = decrypt(encrypted, key);
		System.out.println(decrypted);
	}
	
	private static String rc4(String plaintext, int mode, Key key) throws Exception {
		Cipher cipher = Cipher.getInstance("RC4");
		cipher.init(mode, key);
		return new String(cipher.doFinal(plaintext.getBytes()));
	}

	public static String encrypt(String plaintext, Key key) throws Exception {
		return rc4(plaintext, Cipher.ENCRYPT_MODE, key);
	}

	public static String decrypt(String ciphertext, Key key) throws Exception {
		return rc4(ciphertext, Cipher.DECRYPT_MODE, key);
	}

	public static Key getKey() throws Exception {
		KeyGenerator keyGen = KeyGenerator.getInstance("RC4");
		SecureRandom sr = new SecureRandom();
		keyGen.init(128, sr);
		return keyGen.generateKey();
	}

}