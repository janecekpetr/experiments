package com.gitlab.janecekpetr.experiments.scraps;

/** Fast because not threadsafe. */
class FastRandom {
	private static final long MULTIPLIER = 0x5DEECE66DL;
    private static final long ADDEND = 0xBL;
    private static final long MASK = (1L << 48) - 1;

    private static final double DOUBLE_UNIT = 0x1.0p-53; // 1.0 / (1L << 53)

    private static long seedUniquifier = 8682522807148012L;
    
    private long seed;
    
    public FastRandom() {
        this(seedUniquifier() ^ System.nanoTime());
    }

    private static long seedUniquifier() {
        // L'Ecuyer, "Tables of Linear Congruential Generators of
        // Different Sizes and Good Lattice Structure", 1999
        seedUniquifier = seedUniquifier * 181783497276652981L;
		return seedUniquifier;
    }

    public FastRandom(long seed) {
		this.seed = initialScramble(seed);
    }

    private static long initialScramble(long seed) {
        return (seed ^ MULTIPLIER) & MASK;
    }

	protected int next(int bits) {
        seed = (seed * MULTIPLIER + ADDEND) & MASK;
        return (int)(seed >>> (48 - bits));
    }

	public int nextInt() {
        return next(32);
    }

	public int nextInt(int bound) {
        if (bound <= 0) {
			throw new IllegalArgumentException("bound must be positive");
		}

        int r = next(31);
        int m = bound - 1;
        if ((bound & m) == 0) {
			r = (int)((bound * (long)r) >> 31);
		} else {
            for (int u = r; u - (r = u % bound) + m < 0; u = next(31)) {
				// nothing to do
			}
        }
        return r;
    }

	public long nextLong() {
        // it's okay that the bottom word remains signed.
        return ((long)(next(32)) << 32) + next(32);
    }

	public boolean nextBoolean() {
        return next(1) != 0;
    }

	public float nextFloat() {
        return next(24) / ((float)(1 << 24));
    }

	public double nextDouble() {
        return (((long)(next(26)) << 27) + next(27)) * DOUBLE_UNIT;
    }
}