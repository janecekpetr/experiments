package com.gitlab.janecekpetr.experiments.scraps;

import java.math.BigInteger;
import java.util.Scanner;

public class FermatPrimalityTest {
	private static final BigInteger ZERO = BigInteger.valueOf(0);
	private static final BigInteger ONE = BigInteger.valueOf(1);
	private static final BigInteger TWO = BigInteger.valueOf(2);

	public static void main(String[] args) {
		int n = 0;
		try (Scanner scan = new Scanner(System.in)) {
			System.out.print("Write a number (Fermat num. exponent): ");
			n = scan.nextInt();
		}
		if (n <= 1) {
			System.out.println("Exponent must be greater than one.");
			return;
		}
		long time = System.currentTimeMillis();

		final int k = 1 << n;	// 2^n
		final BigInteger fermat = ONE.shiftLeft(k).add(ONE);	// 2^k + 1
		final int c = (1 << (n-1)) - 1;	// 2^(n-1) - 1
		BigInteger s = BigInteger.valueOf(8);
		for (int i = 0; i < c; i++) {
			// (s^4 - 4*s^2 + 2) mod fermat
			final BigInteger b = s.modPow(TWO, fermat);
			s = b.pow(2).subtract(b.shiftLeft(2)).add(TWO);
		}
		s = s.mod(fermat);
		
		if (s.equals(ZERO)) {
			System.out.println("prime");
		} else {
			System.out.println("composite");
		}
		
		System.out.println(System.currentTimeMillis() - time + " ms");
	}
}