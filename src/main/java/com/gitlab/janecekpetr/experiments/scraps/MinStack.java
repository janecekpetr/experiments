package com.gitlab.janecekpetr.experiments.scraps;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.OptionalInt;

class MinStack {

	private final Deque<ValueAndMin> stack = new ArrayDeque<>();

	public void push(int value) {
		if (stack.isEmpty()) {
			stack.addLast(new ValueAndMin(value, value));
			return;
		}

		int currentMin = stack.peekLast().currentMin();
		int newMin = Math.min(value, currentMin);
		stack.addLast(new ValueAndMin(value, newMin));
	}

	public OptionalInt pop() {
		if (stack.isEmpty()) {
			return OptionalInt.empty();
		}

		ValueAndMin valueAndMin = stack.removeLast();
		return OptionalInt.of(valueAndMin.value());
	}

	public OptionalInt min() {
		if (stack.isEmpty()) {
			return OptionalInt.empty();
		}

		ValueAndMin valueAndMin = stack.peekLast();
		return OptionalInt.of(valueAndMin.currentMin());
	}

	private record ValueAndMin(int value, int currentMin) {}
}