package com.gitlab.janecekpetr.experiments.scraps;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.stream.LongStream;

import com.google.common.base.MoreObjects;
import com.google.common.base.Stopwatch;

public class RandomStringOracle {
	
	private static class SeedAndBase {
		public final long seed;
		public final int base;

		public SeedAndBase(long seed, int base) {
			this.seed = seed;
			this.base = base;
		}
		
		@Override
		public String toString() {
			return MoreObjects.toStringHelper(this)
				.add("seed", seed)
				.add("base", base)
				.toString();
		}
	}

	private static final int ALPHABET_LENGTH = 26 + 1;	// + 1 to accomodate for \0, the termination char
	private static final char ALPHABET_START = 'A' - 1;	// so that 'A' == 1

	public static void main(String[] args) {
		Stopwatch stopwatch = Stopwatch.createStarted();
		try {
			generate("hello");
			generate("Darion");
			generate("Thompson");
			generate("Congratulations");
		} finally {
			System.out.println("Took " + stopwatch + ".");
		}
	}

	private static void generate(String goal) {
		Optional<SeedAndBase> optionalSeed = generateSeed(goal);
		if (optionalSeed.isPresent()) {
			SeedAndBase seedAndBase = optionalSeed.get();
			System.out.println(randomString(seedAndBase) + " => " + seedAndBase);
		} else {
			System.out.println(goal + " not found :/");
		}
		System.out.println();
	}

	private static Optional<SeedAndBase> generateSeed(String goal) {
		return generateSeed(goal, Long.MIN_VALUE, Long.MAX_VALUE);
	}

	public static Optional<SeedAndBase> generateSeed(String goal, long start, long finish) {
		goal = goal.toUpperCase();
		int[] goalArray = goalStringToArray(goal);
		System.out.println("Goal '" + goal + "' = " + Arrays.toString(goalArray));
		
		return LongStream.rangeClosed(start, finish)
			.parallel()
			.peek(seed -> {
				if (seed % 1_000_000_000 == 0) {
					System.out.println(seed);
				}
			})
			.mapToObj(seed -> {
				FastRandom random = new FastRandom(seed);

				int base = getBase(goalArray, random);

				for (int i = 1; i < goalArray.length; i++) {
					int candidate = base + random.nextInt(ALPHABET_LENGTH);
					if (candidate != goalArray[i]) {
						return null;
					}
				}
				return new SeedAndBase(seed, base);
			})
			.filter(Objects::nonNull)
			.findAny();
	}

	private static int getBase(int[] goalArray, FastRandom random) {
		int firstGoalChar = goalArray[0];
		int firstCandidate = random.nextInt(ALPHABET_LENGTH);
		return firstGoalChar - firstCandidate;
	}

	private static int[] goalStringToArray(String goal) {
		char[] input = goal.toCharArray();
		int[] result = new int[input.length + 1];	// zero-padded at the end
		for (int i = 0; i < input.length; i++) {
			result[i] = (char)(input[i] - ALPHABET_START);
		}
		return result;
	}

	public static String randomString(SeedAndBase seedAndBase) {
		Random random = new Random(seedAndBase.seed);
		
		StringBuilder builder = new StringBuilder();
		while (true) {
			int c = seedAndBase.base + random.nextInt(ALPHABET_LENGTH);
			if (c == 0) {
				break;
			}
			builder.append((char)(ALPHABET_START + c));
		}
		return builder.toString();
	}
}