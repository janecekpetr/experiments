package com.gitlab.janecekpetr.experiments.gui.circle;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

class CircleCanvas extends JPanel {

	private int radius = 100;
	private Color color = Color.BLACK;

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(color);
		final int diameter = radius * 2;
		g.drawOval((getWidth() / 2) - radius, (getHeight() / 2) - radius, diameter, diameter);
	}

	public void setCircleRadius(int radius) {
		this.radius = radius;
		repaint();
	}

	public void setCircleColor(Colors color) {
		this.color = color.getColor();
		repaint();
	}

	public int getMaxRadius() {
		return Math.min(getWidth(), getHeight()) / 2;
	}

}