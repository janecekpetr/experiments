package com.gitlab.janecekpetr.experiments.gui.circle;

import java.awt.BorderLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSlider;

class ResizableCirclePanel extends JPanel {

	private final CircleCanvas circleCanvas;
	private final JSlider circleRadiusSlider;
	private final JComboBox<Colors> circleColorSelect;

	public ResizableCirclePanel() {
		setLayout(new BorderLayout());

		// Circle canvas
		circleCanvas = new CircleCanvas();
		circleCanvas.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				canvasResized();
			}
		});
		add(circleCanvas, BorderLayout.CENTER);

		// Circle radius slider
		circleRadiusSlider = new JSlider();
		circleRadiusSlider.addChangeListener(_ -> radiusChanged());
		add(circleRadiusSlider, BorderLayout.SOUTH);

		// Circle color select
		circleColorSelect = new JComboBox<>(Colors.values());
		circleColorSelect.addItemListener(e -> colorSelected((Colors)e.getItem()));
		add(circleColorSelect, BorderLayout.NORTH);
	}

	private void canvasResized() {
		circleRadiusSlider.setMaximum(circleCanvas.getMaxRadius());
	}

	private void radiusChanged() {
		circleCanvas.setCircleRadius(circleRadiusSlider.getValue());
	}

	private void colorSelected(Colors color) {
		circleCanvas.setCircleColor(color);
	}

}