package com.gitlab.janecekpetr.experiments.gui.circle;

import java.awt.Color;

enum Colors {
	BLACK	(Color.BLACK),
	BLUE	(Color.BLUE),
	GREEN	(Color.GREEN),
	RED		(Color.RED);
	
	private final Color color;
	
	private Colors(Color color) {
		this.color = color;
	}
	
	public Color getColor() {
		return color;
	}
	
	@Override
	public String toString() {
		String name = name();
		return name.charAt(0) + name.substring(1).toLowerCase();
	}
}