package com.gitlab.janecekpetr.experiments.gui.circle;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

public class ResizableCircleDemo {

	/** Launch the application. */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception _) {
			// should never happen
		}
		EventQueue.invokeLater(ResizableCircleDemo::new);
	}

	private final JFrame window;

	/** Create the application window. */
	private ResizableCircleDemo() {
		window = new JFrame("Resizable Circle Experiment");
		window.setBounds(100, 100, 600, 400);
		window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		window.setContentPane(new ResizableCirclePanel());
		window.setVisible(true);
	}

}